# Introduction

`extfix` is a command-line program that recursively descends into directory trees and fixes invalid file extensions. It does this by comparing the starting bytes of each file with a list of known file signatures. It does not try to guess at file types beyond this; this means that files without a magic number, such as text files, or file types whose magic number is ambiguous, such as Webm/MKV, are not handled.

# Usage

```
extfix [FLAGS] [OPTIONS] [directories]...
```

If no directories are specified, it will search in the current directory.

# Flags

```
-a, --all                Do not skip hidden files and directories.
-n, --dry-run            Dry run. Print filenames only; do not modify them.
-e, --extended           Perform additional transformations: remove Twitter suffixes and redundant extensions
-L, --follow-symlinks    Follow symbolic links.
-h, --help               Prints help information
-i, --interactive        Interactive mode. Prompt before renaming files. Note that extfix will prompt before
                         overwriting files even if this option is not set.
-V, --version            Prints version information
-v, --verbose            Print filenames as they are encountered.
```

# Options

```
-d, --days <days>    Like --time, but specified in days instead of hours.
-t, --time <time>    Skip files with an mtime greater than the one given in this option, in hours. If unset or set to 0, process all files.
```

# TODO

`extfix` is a work in progress.

- [x] Respect --time, --days
- [x] Respect directory args
- [x] Respect --dry-run
- [x] Respect -L
- [x] Respect --all
- [x] Add a bunch of filetypes
- [x] Respect wildcards
- [x] Interactive mode
- [x] Prompt if replacement filename already exists. Provide options to either
remove the incorrect file or to overwrite the correct one anyway.
- [x] Fix other kinds of broken file extensions, like double extensions or Twitter's suffixes. Do this by assuming UTF-8 filenames when -e is provided.
- [x] Don't mess with Emacs-style swap files (files ending in ~).
- [ ] Recognize tar files, compressed tar files?
- [ ] Recognize ZIP files?
