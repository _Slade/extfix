use clap::{App, Arg};

use std::ffi::OsString;
use std::time::Duration;

#[derive(Debug)]
pub struct Opts {
    pub dirs: Vec<OsString>,
    pub dry_run: bool,
    pub extended: bool,
    pub follow: bool,
    pub interactive: bool,
    pub max_time_delta: Duration,
    pub skip_hidden: bool,
    pub verbose: bool,
}

pub fn get() -> Opts {
    let validate_time = |t: String| match t.parse::<f64>() {
        Ok(time) if time >= 0.0 => Ok(()),
        Ok(_) => Err("Time must not be negative".to_owned()),
        _ => Err("Unable to parse time value".to_owned()),
    };
    let matches: clap::ArgMatches<'static> = App::new("extfix")
        .version("0.1.0")
        .about(
            "Recursively walks a directory tree and fixes broken extensions.",
        )
        .arg(Arg::with_name("all")
            .short("a")
            .long("all")
            .help("Do not skip hidden files and directories.")
            .takes_value(false))
        .arg(Arg::with_name("days")
            .short("d")
            .long("days")
            .help("Like --time, but specified in days instead of hours.")
            .takes_value(true)
            .validator(validate_time))
        .arg(Arg::with_name("directories")
            .index(1)
            .multiple(true)
            .help("Set the starting directory or directories.")
            .default_value("."))
        .arg(Arg::with_name("dry-run")
            .short("n")
            .long("dry-run")
            .help(
                "Dry run. Show which files would be renamed (and what they'd \
                be renamed to), without actually renaming them."
            )
            .takes_value(false))
        .arg(Arg::with_name("extended")
            .short("e")
            .long("extended")
            .help(
                "Perform additional transformations: remove Twitter \
                suffixes and redundant extensions. Paths are assumed to be \
                valid UTF-8 if this option is enabled."
            )
            .takes_value(false))
        .arg(Arg::with_name("follow")
            .short("L")
            .long("follow-symlinks")
            .help("Follow symbolic links.")
            .takes_value(false))
        .arg(Arg::with_name("interactive")
            .short("i")
            .long("interactive")
            .help(
                "Interactive mode. Prompt before renaming files. Note that \
                extfix will prompt before overwriting files even if this \
                option is not set."
            )
            .takes_value(false))
        .arg(Arg::with_name("time")
            .short("t")
            .long("time")
            .help(
                "Skip files with an mtime greater than the one given in this \
                option, in hours. If unset or set to 0, process all files."
            )
            .takes_value(true)
            .conflicts_with("days")
            .validator(validate_time))
        .arg(Arg::with_name("verbose")
            .short("v")
            .long("verbose")
            .help("Print the paths of files as they are encountered.")
            .takes_value(false))
        .get_matches();
    let dirs = match matches.values_of_os("directories") {
        Some(values) => values.map(|s| s.to_owned()).collect(),
        None => vec![OsString::from(".")],
    };
    let time = matches
        .value_of("time")
        .unwrap_or(matches.value_of("days").unwrap_or("0.0"));
    Opts {
        dirs,
        max_time_delta: get_time_delta(time, matches.is_present("days")),
        dry_run: matches.is_present("dry-run"),
        extended: matches.is_present("extended"),
        follow: matches.is_present("follow"),
        interactive: matches.is_present("interactive"),
        skip_hidden: matches.is_present("all"),
        verbose: matches.is_present("verbose"),
    }
}

fn get_time_delta(raw_opt: &str, is_days: bool) -> Duration {
    let time = raw_opt.parse::<f64>().unwrap();
    let coeff = 60.0 * 60.0 * if is_days { 24.0 } else { 1.0 };
    Duration::from_secs((time * coeff) as u64)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_time_delta() {
        assert_eq!(get_time_delta("0.0", false), Duration::from_secs(0));
        assert_eq!(get_time_delta("1.0", false), Duration::from_secs(3600));
        assert_eq!(get_time_delta("1.5", false), Duration::from_secs(5400));
        assert_eq!(get_time_delta("2.0", false), Duration::from_secs(7200));
    }

    #[test]
    fn test_get_days_delta() {
        assert_eq!(get_time_delta("0.0", true), Duration::from_secs(0));
        assert_eq!(get_time_delta("1.0", true), Duration::from_secs(86400));
        assert_eq!(get_time_delta("1.5", true), Duration::from_secs(129600));
        assert_eq!(get_time_delta("2.0", true), Duration::from_secs(172800));
    }

}
