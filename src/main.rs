mod filetype;
mod opts;
mod transformations;

use anyhow::Result;
use filetype::FileType;
use std::ffi::{OsStr, OsString};
use std::fs::{remove_file, rename, File};
use std::io::{self, Write};
use std::path::Path;
use std::time::{Duration, SystemTime};
use transformations::{remove_duplicate_extensions, remove_twitter_suffixes};
use walkdir::{DirEntry, WalkDir};

struct Extensions<'a> {
    actual: &'a OsStr,
    expected: OsString,
}

/// Action to perform on a path.
enum Action {
    /// Rename the file, overwriting whatever might be at its destination.
    Rename,
    /// Unlink the file.
    Unlink,
    /// Do nothing.
    Skip,
}

macro_rules! print_paths {
    ($fmt:expr, $old:expr, $new:expr) => {{
        if let Some(filename) = $new.file_name() {
            print!($fmt, $old.display(), filename.to_string_lossy());
        } else {
            print!($fmt, $old.display(), $new.display());
        }
    }};

    ($old:expr, $new:expr) => {
        print_paths!("'{}' -> '{}'\n", $old, $new)
    };
}

fn main() {
    let now = SystemTime::now();
    let opts = opts::get();
    let mut processed = 0;
    let mut total = 0;
    for dir in &opts.dirs {
        for entry in WalkDir::new(dir)
            .follow_links(opts.follow)
            .into_iter()
            .filter_entry(|e| {
                if e.file_type().is_file() {
                    total += 1;
                }
                !skip(e, &opts, now)
            })
        {
            match entry {
                Ok(entry) => {
                    if entry.file_type().is_file() {
                        if opts.verbose {
                            println!("{}", entry.path().display());
                        }
                        process_entry(entry.path(), &opts);
                        processed += 1;
                    }
                }
                Err(e) => eprintln!("Error finding files: {}", e),
            }
        }
    }
    // TODO Next stable release should allow .as_secs_f64(); see issue #54361
    let end = SystemTime::now();
    let duration = end.duration_since(now).unwrap();
    println!(
        "Processed {} files (skipped {}) in {}ms; rate = {:.2}/s",
        total,
        total - processed,
        duration.as_millis(),
        1e9 * ((total as f64) / (duration.as_nanos() as f64))
    );
}

fn process_entry(path: &Path, opts: &opts::Opts) {
    if path.ends_with("~") {
        return; // Skip Emacs-style backup files
    }
    let old_path = path;
    let mut new_path = old_path.clone();
    let new_path_owned;
    if opts.extended {
        new_path = remove_twitter_suffixes(new_path);
        new_path = remove_duplicate_extensions(new_path);
    }
    if let Some(extensions) = get_extensions(old_path) {
        if extensions.actual != extensions.expected {
            new_path_owned = new_path.with_extension(extensions.expected);
            new_path = &new_path_owned;
        }
    }
    if old_path != new_path {
        if opts.dry_run {
            print_paths!(old_path, new_path);
        } else {
            match interactive_rename(old_path, new_path, opts.interactive) {
                Ok(Action::Rename) => {
                    print_paths!(old_path, new_path);
                }
                Ok(Action::Unlink) => {
                    println!("Removed '{}'", old_path.display());
                }
                Ok(Action::Skip) => {}
                Err(e) => eprintln!("Error renaming file: {}", e),
            }
        }
    }
}

/**
 * Renames a file to fix its extension. Returns a result with an Ok value
 * indicating which `Action` was taken.
 *
 * In interactive mode this function will read from standard input to confirm
 * the operation. It will also prompt before overwriting existing files
 * regardless of whether `interactive` is true.
 *
 * This is the only part of the program that should be able to do anything
 * destructive (renaming or deleting files).
 */
fn interactive_rename(
    old_path: &Path,
    new_path: &Path,
    interactive: bool,
) -> Result<Action> {
    let action = if new_path.exists() {
        prompt_to_overwrite(new_path)?
    } else if interactive {
        prompt_to_rename(old_path, new_path)?
    } else {
        Action::Rename
    };
    match action {
        Action::Rename => {
            rename(old_path, new_path)?;
        }
        Action::Unlink => {
            remove_file(old_path)?;
        }
        Action::Skip => {}
    };
    Ok(action)
}

fn skip(entry: &DirEntry, opts: &opts::Opts, now: SystemTime) -> bool {
    if opts.skip_hidden && is_hidden(entry) {
        return true;
    }
    if entry.file_type().is_dir() || opts.max_time_delta.as_secs() == 0 {
        return false;
    }
    match check_mtime(entry, opts.max_time_delta, now) {
        Ok(skip) => skip,
        Err(_) => {
            eprintln!(
                "Error: Could not get info for '{}'",
                entry.path().display()
            );
            false
        }
    }
}

fn check_mtime(
    entry: &DirEntry,
    max_time_delta: Duration,
    now: SystemTime,
) -> Result<bool> {
    let mtime = entry.metadata()?.modified()?;
    Ok(match now.duration_since(mtime) {
        Ok(delta) => delta > max_time_delta,
        Err(_) => true, // Skip files newer than when we started
    })
}

fn is_hidden(entry: &DirEntry) -> bool {
    entry
        .file_name()
        .to_str()
        .map(|s| s.starts_with("."))
        .unwrap_or(false)
}

fn get_extensions(path: &Path) -> Option<Extensions> {
    let extension = path.extension().unwrap_or(OsStr::new(""));
    match File::open(path) {
        Ok(mut file) => match FileType::get(&mut file) {
            Ok(Some(filetype)) => {
                return Some(Extensions {
                    actual: extension,
                    expected: filetype.extension().to_owned(),
                });
            }
            Ok(None) => {}
            Err(e) => eprintln!("Error: Could not read file: {}", e),
        },
        Err(e) => eprintln!(
            "Error: Could not open {} for reading: {}",
            path.display(),
            e
        ),
    };
    None
}

fn prompt_to_rename(old_path: &Path, new_path: &Path) -> Result<Action> {
    print_paths!("Rename '{}' to '{}'? ", old_path, new_path);
    io::stdout().flush()?;
    let response = get_response()?;
    Ok(match response.as_ref() {
        "y" | "yes" => Action::Rename,
        _ => Action::Skip,
    })
}

fn prompt_to_overwrite(path: &Path) -> Result<Action> {
    print!(
        "Destination '{}' already exists.\n\t(1) Overwrite?\
         \n\t(2) Remove incorrect file\n\t(Enter) Do nothing\n> ",
        path.display(),
    );
    io::stdout().flush()?;
    let response = get_response()?;
    Ok(match response.as_ref() {
        "1" => Action::Rename,
        "2" => Action::Unlink,
        _ => Action::Skip,
    })
}

fn get_response() -> Result<String> {
    let mut buf = String::new();
    io::stdin().read_line(&mut buf)?;
    buf.make_ascii_lowercase();
    return Ok(buf.trim().to_owned());
}
