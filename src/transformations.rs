use crate::filetype::FileType;
use lazy_static::lazy_static;
use regex::Regex;
use std::collections::HashSet;
use std::path::Path;
use strum::IntoEnumIterator;

lazy_static! {
    static ref TWITTER_SUFFIXES: Regex = Regex::new(
        r"(?x)
        ^
        (
            .+                 # Path and filename
            \. (?: png | jpg ) # Extension
        )
        (?: :(?:small|large|orig) ) # Twitter suffix
        (?: \. (?: png | jpg ))?
        $
    "
    )
    .unwrap();
    static ref KNOWN_SUFFIXES: HashSet<String> = FileType::iter()
        .map(|f| f.extension().to_str().unwrap().to_owned())
        .collect();
    static ref IGNORED_SUFFIXES: HashSet<&'static str> = [
        "orig", "bak", "gz", "xz", "lz", "lzo", "z", "Z", "lzma", "bz2", "zip"
    ]
    .iter()
    .cloned()
    .collect();
}

/**
 * Removes :small, :large, and :orig suffixes appended by Twitter. Assumes
 * UTF-8 filename.
 */
pub fn remove_twitter_suffixes(path: &Path) -> &Path {
    if let Some(stripped) = path
        .to_str()
        .and_then(|path_str| TWITTER_SUFFIXES.captures(path_str))
        .and_then(|captures| captures.get(1))
        .map(|matched| matched.as_str())
    {
        return Path::new(stripped);
    }
    return path;
}

/**
 * Removes duplicate extensions, leaving only the first one. For example,
 * `foo/bar.png.jpg` will become `foo/bar.png`. Assumes UTF-8 filename.
 *
 * The reason for leaving only the first extension is for performance. This way
 * the returned path can reference a slice of the original path string; the
 * path itself does not need to be cloned unless and until the extension has to
 * be replaced.
 *
 * Only combinations of extensions that are likely to be erroneous are
 * replaced. This includes:
 * * Multiple instances of the same extension: `foo.png.png`
 * * Nonsensical combinations: `foo.png.jpg`, `foo.ogg.mp3`, etc.
 *
 * Some duplicate extensions are left alone:
 * * `foo.tar.*`
 * * `foo.*.bak`
 * * `foo.*.orig`
 * * `foo.*.{gz,xz,lz,lzo,z,Z,lzma,bz2,zip}`
 */
pub fn remove_duplicate_extensions<'a>(path: &'a Path) -> &'a Path {
    if let Some(path_str) = path.to_str() {
        let mut stripped = path_str;
        let mut last_stripped = path_str;
        loop {
            if let Some((filename, extension)) = split_extension(stripped) {
                if IGNORED_SUFFIXES.contains(extension)
                    || !KNOWN_SUFFIXES.contains(extension)
                    || filename.ends_with(".tar")
                {
                    break;
                }
                last_stripped = stripped;
                stripped = filename;
            } else {
                break;
            }
        }
        Path::new(last_stripped)
    } else {
        path
    }
}

fn split_extension<'a>(path_str: &'a str) -> Option<(&'a str, &'a str)> {
    let results: Vec<&str> = path_str.rsplitn(2, '.').collect();
    if results.len() == 2 && !results[0].is_empty() && !results[1].is_empty() {
        Some((results[1], results[0]))
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use pretty_assertions::assert_eq;

    #[test]
    fn test_split_extension() {
        let path = Path::new("./foo/bar/baz.png.jpg");
        let (filename, extension) =
            split_extension(path.to_str().unwrap()).unwrap();
        assert_eq!(filename, "./foo/bar/baz.png");
        assert_eq!(extension, "jpg");

        let path = Path::new("./foo/bar/baz");
        assert_eq!(
            split_extension(path.to_str().unwrap()),
            None,
            "Should get None if the path has no extension"
        );

        let path = Path::new("./foo/bar/baz.");
        assert_eq!(
            split_extension(path.to_str().unwrap()),
            None,
            "Should get None if the path has an empty extension (ends in dot)"
        );
    }

    #[test]
    fn test_twitter_suffixes() {
        let suffixes = [":small", ":large", ":orig"];
        for suffix in suffixes.iter() {
            let path_str = format!("./foo/bar/abcdefg.jpg{}", suffix);
            let path = Path::new(&path_str);
            let actual = remove_twitter_suffixes(path);
            let expected = Path::new("./foo/bar/abcdefg.jpg");
            assert_eq!(actual, expected, "Should remove Twitter suffixes");
        }

        let path = Path::new("./foo/bar/baz.png:large.png");
        assert_eq!(
            remove_twitter_suffixes(path),
            Path::new("./foo/bar/baz.png"),
            "Should remove Twitter suffixes followed by an extension"
        );

        let path = Path::new("./foo/bar/baz.png");
        assert_eq!(
            remove_twitter_suffixes(path),
            path,
            "Shouldn't remove non-Twitter suffixes"
        );
    }

    #[test]
    fn test_remove_duplicates() {
        let path =
            remove_duplicate_extensions(Path::new("./foo/bar/abcdefg.png.jpg"));
        assert_eq!(
            path,
            Path::new("./foo/bar/abcdefg.png"),
            "Duplicate extension should be removed"
        );

        let path = remove_duplicate_extensions(Path::new(
            "./foo/bar/abcdefg.png.jpg.mp3.mp4",
        ));
        assert_eq!(
            path,
            Path::new("./foo/bar/abcdefg.png"),
            "Multiple duplicate extensions should be removed"
        );

        let path = Path::new("foo.png");
        assert_eq!(
            remove_duplicate_extensions(path),
            path,
            "Single extensions should be left alone"
        );

        let path = Path::new("123456789.foo.png");
        assert_eq!(
            remove_duplicate_extensions(path),
            path,
            "Only known extensions should be removed"
        );

        for &path_str in &["foo.bak", "foo.orig"] {
            let path = Path::new(path_str);
            assert_eq!(
                remove_duplicate_extensions(path),
                path,
                "Backup files should be left alone"
            );
        }

        for &path_str in &["foo.tar.gz", "foo.txt.xz"] {
            let path = Path::new(path_str);
            assert_eq!(
                remove_duplicate_extensions(path),
                path,
                "Compressed files should have duplicate suffixes left alone"
            );
        }
    }

    #[test]
    fn test_transformations() {
        let mut path = Path::new("./foo/bar/baz.jpg.png:large.png");
        path = remove_twitter_suffixes(path);
        assert_eq!(path, Path::new("./foo/bar/baz.jpg.png"));
        path = remove_duplicate_extensions(path);
        assert_eq!(path, Path::new("./foo/bar/baz.jpg"));
    }

    #[test]
    fn test_known_suffixes() {
        assert!(KNOWN_SUFFIXES.len() > 0, "KNOWN_SUFFIXES shouldn't be empty");
    }
}
