use anyhow::Result;
use std::ffi::OsStr;
use std::io::Read;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;

#[derive(Debug, PartialEq, EnumIter)]
pub enum FileType {
    Flac,
    Flv,
    Gif87,
    Gif89,
    Jpeg,
    Mp3,
    Mp4,
    Ogg,
    Pdf,
    Png,
    Swf,
    Tiff,
    Webp,
}

const WILDCARD: u8 = 0x3F; // Use ASCII '?' as a wildcard
const LONGEST_SIGNATURE: usize = 12;

impl FileType {
    /**
     * Gets the filetype of a file by checking its magic number.
     */
    pub fn get(
        file: &mut dyn Read,
    ) -> Result<Option<FileType>, std::io::Error> {
        let mut buf = [0; LONGEST_SIGNATURE];
        file.read(&mut buf)?;
        for filetype in FileType::iter() {
            let mut is_match = true;
            for (i, byte) in filetype.magic().iter().enumerate() {
                if *byte != WILDCARD && buf[i] != *byte {
                    is_match = false;
                    break;
                }
            }
            if is_match {
                return Ok(Some(filetype));
            }
        }
        Ok(None)
    }

    /**
     * File signature magic numbers.
     *
     * All signatures in this module are static bytestring slices. When
     * interpreting them, bytes equal to the WILDCARD constant are to be
     * treated as wildcards.
     *
     * ASCII '?' (0x3F) was chosen as a wildcard because it's not used in any
     * known file signatures and it has a convenient symbolic association.
     *
     * Special cases:
     *
     * * Matroska files (mkv, mka, webm, etc.) are ignored, because the type
     * cannot be identified from their magic numbers.
     */
    fn magic(&self) -> &'static [u8] {
        match *self {
            FileType::Flac => b"\x66\x4C\x61\x43",
            FileType::Flv => b"\x46\x4C\x56\x01",
            FileType::Gif87 => b"\x47\x49\x46\x38\x37\x61",
            FileType::Gif89 => b"\x47\x49\x46\x38\x39\x61",
            FileType::Jpeg => b"\xFF\xD8\xFF",
            FileType::Mp3 => b"\x49\x44\x33",
            FileType::Mp4 => b"\x3F\x3F\x3F\x66\x74\x79\x70\x4D\x53\x4E\x56",
            FileType::Ogg => b"\x4F\x67\x67\x53",
            FileType::Pdf => b"\x25\x50\x44\x46\x2d",
            FileType::Png => b"\x89\x50\x4E\x47\x0D\x0A\x1A\x0A",
            FileType::Swf => b"\x5A\x57\x53",
            FileType::Tiff => b"\x49\x49\x2A",
            FileType::Webp => {
                b"\x52\x49\x46\x46\x3F\x3F\x3F\x3F\x57\x45\x42\x50"
            }
        }
    }

    /**
     * Get the proper extension for this filetype.
     */
    pub fn extension(&self) -> &OsStr {
        OsStr::new(match *self {
            FileType::Flac => "flac",
            FileType::Flv => "flv",
            FileType::Gif87 => "gif",
            FileType::Gif89 => "gif",
            FileType::Jpeg => "jpg",
            FileType::Mp3 => "mp3",
            FileType::Mp4 => "mp4",
            FileType::Png => "png",
            FileType::Swf => "swf",
            FileType::Tiff => "tiff",
            FileType::Ogg => "ogg",
            FileType::Pdf => "pdf",
            FileType::Webp => "webp",
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs::File;
    use std::path::Path;

    fn test_get_filetype(file: &Path, filetype: FileType) {
        let mut fh = File::open(file).expect(
            "Couldn't find test file. This indicates a problem with the test \
             directory rather than the code itself. Make sure you have the \
             test_files directory.",
        );
        match FileType::get(&mut fh) {
            Ok(Some(ref result)) if *result == filetype => {}
            Ok(None) => panic!("Got an unknown filetype"),
            Ok(_) => panic!("Wrong filetype returned"),
            Err(e) => panic!("Failed to read file due to an error: {}", e),
        }
    }

    #[test]
    fn test_get_png_filetype() {
        test_get_filetype(Path::new("test_files/png_file"), FileType::Png);
    }

    #[test]
    fn test_get_jpg_filetype() {
        test_get_filetype(Path::new("test_files/jpg_file"), FileType::Jpeg);
    }

    #[test]
    fn test_get_webp_filetype() {
        test_get_filetype(Path::new("test_files/webp_file"), FileType::Webp);
    }

    #[test]
    fn test_longest_signature() {
        let longest = FileType::iter().fold(0, |acc, ft| {
            let len = ft.magic().len();
            if len > acc {
                len
            } else {
                acc
            }
        });
        assert_eq!(
            LONGEST_SIGNATURE,
            longest,
            "The LONGEST_SIGNATURE constant is incorrect; it's {} but should be {}",
            LONGEST_SIGNATURE,
            longest
        );
    }
}
